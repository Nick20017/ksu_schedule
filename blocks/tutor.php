<form action="../php/addLesson.php" method="POST" id="scheduleForm" onsubmit="checkInput('scheduleForm', event)">
    <label>День</label><br />
    <select name="dayOfWeek" form="scheduleForm">
        <option v-for="day in daysOfWeek">{{ day }}</option>
    </select><br />
    <label for="group">Группа</label><br />
    <input type="text" name="group" placeholder="121" /><br />
    <label for="name">Название предмета</label><br />
    <input type="text" name="name" /><br />
    <label for="tutor">Преподаватель</label><br />
    <input type="text" name="tutor" /><br />
    <label for="place">Место проведения</label><br />
    <input type="text" name="place" /><br />
    <label>Номер пары</label><br />
    <select name="number" form="scheduleForm">
        <option v-for="num in numbers">{{ num }}</option>
    </select><br />
    <input type="submit" name="send" value="Добавить" />
</form>