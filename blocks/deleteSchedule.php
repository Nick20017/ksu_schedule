<form action="/php/deleteSchedule.php" method="POST" id="deleteForm">
    <label>День</label><br />
    <select name="dayOfWeek" form="deleteForm">
        <option value="0" disabled selected>День недели</option>
        <option v-for="day in daysOfWeek">{{ day }}</option>
    </select><br />
    <label for="group">Группа</label><br />
    <input type="text" name="group" placeholder="121" /><br />
    <label for="name">Название предмета</label><br />
    <input type="text" name="name" /><br />
    <label for="tutor">Преподаватель</label><br />
    <input type="text" name="tutor" /><br />
    <label for="place">Место проведения</label><br />
    <input type="text" name="place" /><br />
    <label>Номер пары</label><br />
    <select name="number" form="deleteForm">
        <option value="0" disabled selected>Номер пары</option>
        <option v-for="num in numbers">{{ num }}</option>
    </select><br />
    <input type="submit" name="send" value="Отправить" />
</form>