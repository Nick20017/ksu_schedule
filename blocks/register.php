<form method="POST" action="../php/register.php" id="registerForm" onsubmit="checkInput('registerForm', event)">
    <label for="login">Логин</label><br />
    <input type="text" name="login" placeholder="login" /><br />
    <label for="password">Пароль</label><br />
    <input type="password" name="password" placeholder="password" /><br />
    <label for="email">E-mail</label><br />
    <input type="text" name="email" placeholder="name@gmail.com" /><br />
    <label for="phone">Номер телефона</label><br />
    <input type="text" name="phone" placeholder="+380#########" /><br />
    <label>Статус</label><br />
    <select @change="change($event.target.value)" name="status" id="status">
        <option>Студент</option>
        <option>Преподаватель</option>
    </select><br />
    <label v-if="isStudent" for="group">Номер группы</label><br v-if="isStudent" />
    <input v-if="isStudent" type="text" name="group" placeholder="121" /><br />
    <label for="birthday">Дата рождения</label><br />
    <input type="date" name="birthday" /><br />
    <input type="submit" name="send" value="Зарегистрироваться" /><br />
    <a href="../login">Уже есть аккаунт? Нажмите, чтобы войти</a>
</form>