<ol v-if="isMobile">
    <li v-for="temp in sc">
        <ul>
            <li v-for="element in temp">{{ element }}</li>
        </ul>
    </li>
</ol>
<table align="center" v-else>
    <tr>
        <th>День</th>
        <th>Предмет</th>
        <th>Преподаватель</th>
        <th>Место проведения</th>
        <th>Номер пары</th>
        <th>Время проведения</th>
        <th>Группа</th>
    </tr>
    <tr v-for="temp in sc">
        <td v-for="element in temp">{{ element }}</td>
    </tr>
</table>