<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Авторизация</title>

        <link href="../css/style.css" type="text/css" rel="stylesheet" />
        <link href="../css/logreg.css" type="text/css" rel="stylesheet" />

        <script src="../js/checkInput.js"></script>
    </head>

    <body>
        <div id="header">
            <h1>Авторизация</h1>
        </div>
        <div id="content">
            <?php include_once("../blocks/login.php"); ?>
        </div>
    </body>
</html>