<?php
    ini_set('display_errors', true);
    if(!isset($_COOKIE['login']) || !isset($_COOKIE['password']) || !isset($_COOKIE['group'])) {
        header('Location: /login');
    }

    if($_COOKIE['group'] != 0)
        header('Location: /');
?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8" />
        <title>Удаление расписания</title>

        <link href="../../css/style.css" type="text/css" rel="stylesheet" />
        <link href="/css/main.css" type="text/css" rel="stylesheet" />

        <script src="../../js/vue/vue.min.js"></script>
        <script src="../../js/mobile-detect/mobile-detect.min.js"></script>
        <script src="../../js/checkInput.js"></script>
        <script defer src="../../js/tutor.js"></script>
    </head>

    <body>
        <div id="header"><h1>Кабинет преподавателя - Удаление расписания</h1></div>
        <div id="left">
            <a href="../../" id="main">Расписание</a><br /><br />
            <a href="../" id="tutor">Кабинет преподавателя</a>
        </div>
        <script>
            if(new MobileDetect(window.navigator.userAgent).mobile() != null)
                document.write("<br />");
        </script>
        <div id="right">
            <a href="../../php/logout.php" id="logout">Выйти</a>
        </div>
        <div id="content">
            <?php require_once("../../blocks/deleteSchedule.php"); ?>
        </div>
    </body>
</html>