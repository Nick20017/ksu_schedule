var s = new Array(new Array(), new Array(), new Array(), new Array(), new Array(), new Array(), new Array());
for(var i = 0; i < this.sch.length; i++) {
    s[i] = this.sch[i].split(';');
}

function loadSchedule(item, group) {
    var daily = new Array();

    for(var i = 0; i < s[0].length; i++) {
        if(s[0][i] == item && s[6][i] == group) {
            daily.push(new Array(s[0][i], s[1][i], s[2][i], s[3][i], s[4][i], s[5][i], s[6][i]))
        }
    }

    return daily;
}

var Groups = new Array();
Groups = g.split(';');

this.isMobile = window.matchMedia("(max-width: 1000px)").matches;

var content = new Vue({
    el: '#content',
    data: {
        sc: loadSchedule("Понедельник", Groups[0]),
        isMobile: this.isMobile
    },
    methods: {
        loadSchedule(day, group) {
            this.sc = loadSchedule(day, group);
        },

        setIsMobile(value) {
            this.isMobile = value;
        }
    }
});

setInterval(function() {
    content.setIsMobile(window.matchMedia("(max-width: 1000px)").matches);
}, 100);

var left = new Vue({
    el: '#left',
    data: {
        daysOfWeek: [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница'
        ],
        groups: Groups,
        isTutor: isTutor,
        day: "Понедельник",
        group: Groups[0]
    },
    methods: {
        changeDay(value) {
            this.day = value;
            content.loadSchedule(this.day, this.group);
        },

        changeGroup(value) {
            this.group = value;
            content.loadSchedule(this.day, this.group);
        }
    }
});

var right = new Vue ({
    el: '#right',
    data: {
        isTutor: isTutor
    }
});