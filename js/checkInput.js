function checkInput(id, event) {
    var block = document.getElementById(id);
    var elements = block.children;
    var error = false;
    for(var i = 0; i < elements.length; i++) {
        if(elements[i].getAttribute("type") != ""
        && elements[i].getAttribute("type") != "submit"
        && elements[i].getAttribute("type") != "button"
        && elements[i].tagName == "INPUT") {
            if(elements[i].value == "") {
                elements[i].style.backgroundColor = "red";
                error = true;
            } else {
                elements[i].style.backgroundColor = "white";
            }
        }
    }

    if(error) {
        event.preventDefault();
        return false;
    }
}