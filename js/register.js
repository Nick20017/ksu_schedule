var content = new Vue({
    el: '#content',
    data: {
        isStudent: true
    },
    methods: {
        change(item) {
            if(item == "Студент")
                this.isStudent = true;
            else
                this.isStudent = false;
        }
    }
});