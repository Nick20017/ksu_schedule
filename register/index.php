<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Регистрация</title>

        <link href="../css/style.css" type="text/css" rel="stylesheet" />
        <link href="../css/logreg.css" type="text/css" rel="stylesheet" />

        <script src="../js/vue/vue.min.js"></script>
        <script defer src="../js/register.js"></script>
        <script src="../js/checkInput.js"></script>
    </head>

    <body>
        <div id="header">
            <h1>Регистрация</h1>
        </div>
        <div id="content">
            <?php include_once("../blocks/register.php"); ?>
        </div>
    </body>
</html>