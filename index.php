<?php
    ini_set('display_errors', true);
    
    if(!isset($_COOKIE['login']) || !isset($_COOKIE['password']) || !isset($_COOKIE['group'])) {
        header('Location: login');
    }

    $istutor = false;
    
    if($_COOKIE['group'] == 0)
        $istutor = true;

    include_once("php/getGroups.php");
?>
<?php include_once("php/loadSchedule.php"); ?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8" />
        <title>Расписание занятий ХДУ</title>

        <link href="css/style.css" type="text/css" rel="stylesheet" />
        <link href="css/main.css" type="text/css" rel="stylesheet" />

        <script src="js/vue/vue.min.js"></script>
        <script src="js/mobile-detect/mobile-detect.min.js"></script>
        <script type="text/javascript">
            var sch = new Array(); // Schedule
            var i = 0;
            <?php
                for($i = 0; $i < count($schedule); $i++) {
            ?>
                    sch.push('<?php echo implode(';', $schedule[$i]); ?>');
                    i++;
            <?php
                } ?>

            var isTutor = <?php echo $istutor ? 'true' : 'false'; ?>;

            var g = null;
            g = '<?php echo implode(';', $groups); ?>';
        </script>
        <script>
            var detector = new MobileDetect(window.navigator.userAgent);
            var isMobile = null;

            if(detector.mobile() == null)
                isMobile = false;
            else
                isMobile = true;
        </script>
        <script defer src="js/main.js"></script>
	<script charset="UTF-8" src="//web.webpushs.com/js/push/4bbdcb363b332b1824f97714833a742c_0.js" async></script>
    </head>

    <body>
        <div id="header"><h1>Расписание занятий ХДУ</h1></div>
        <div id="left">
            <select @change="changeDay($event.target.value)">
                <option v-for="day in daysOfWeek">{{ day }}</option>
            </select>
            <select v-if="isTutor" @change="changeGroup($event.target.value)">
                <option v-for="group in groups">{{ group }}</option>
            </select>
        </div>
        <div id="right">
            <div v-if="isTutor"><a href="tutor" id="tutor">Кабинет преподавателя</a><br /><br /></div>
            <a href="php/logout.php" id="logout">Выйти</a>
        </div>
        <div id="content">
            <?php require_once("blocks/schedule.php"); ?>
        </div>
    </body>
</html>